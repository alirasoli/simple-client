#include "parser.h"

#include <QBuffer>
#include <QDataStream>

Parser::Parser(const QByteArray &req)
{
    QByteArray data = req;
    QBuffer buffer(&data);
    buffer.open(QIODevice::ReadOnly);

    QDataStream dataStream(&buffer);
    dataStream >> _code >> _content;
}

Parser::Parser(qint32 code, const QByteArray &content)
{
    _code = code;
    _content = content;
}

QByteArray Parser::getRequest()
{
    QByteArray request;
    QDataStream requestStream(&request, QIODevice::WriteOnly);
    requestStream << _code;
    requestStream << _content;

    return request;
}

qint32 Parser::getCode() const
{
    return _code;
}

QByteArray Parser::getContent() const
{
    return _content;
}
