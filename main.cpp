#include "parser.h"

#include <QCoreApplication>
#include <QObject>
#include <qtcpsocket.h>
#include <iostream>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QTcpSocket *socket = new QTcpSocket;
    QObject::connect(socket, &QTcpSocket::connected, [socket](){
        qDebug() << "successfully connected to host";
        qDebug() << "Enter your name:";
        std::string line;
        std::getline(std::cin, line);
        QString name = QString::fromStdString(line);

        Parser nameReq(Parser::SetName, name.toUtf8());
        socket->write(nameReq.getRequest());


    });
    QObject::connect(socket, &QTcpSocket::readyRead, [socket](){
        Parser response(socket->readAll());
        qDebug() << QString(response.getContent());
    });
    socket->connectToHost("127.0.0.1", 3000);

    return a.exec();
}
