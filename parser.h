#ifndef PARSER_H
#define PARSER_H

#include <QObject>

class Parser
{
public:
    enum Request {
        SetName,
        PublicMessage
    };

    enum Response {
        Message
    };

    Parser(const QByteArray &req);
    Parser(qint32 code, const QByteArray &content);

    QByteArray getRequest();

    qint32 getCode() const;
    QByteArray getContent() const;

private:
    qint32 _code;
    QByteArray _content;
};

#endif // PARSER_H
